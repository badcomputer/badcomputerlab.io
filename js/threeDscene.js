var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera(75, containerWidth / containerHeight, 0.1, 1000);
var renderer = new THREE.WebGLRenderer({ 
    antialias: true,
    alpha: true
} );


var container = document.getElementById('3d-container'); // Get the container element
var containerWidth = 500; // Replace with the actual container width
var containerHeight = 500; // Replace with the actual container height

// Function to update container dimensions
function updateContainerDimensions() {
    containerWidth = window.innerWidth/2;
    containerHeight = window.innerHeight/2;
    
    camera.aspect = containerWidth / containerHeight;
    camera.updateProjectionMatrix();
    
    renderer.setSize(containerWidth, containerHeight);
}

// Initial call to set container dimensions
updateContainerDimensions();

// Listen for the window resize event and update container dimensions
window.addEventListener('resize', function() {
    updateContainerDimensions();
});

renderer.setSize(containerWidth, containerHeight);
document.getElementById('3d-container').appendChild(renderer.domElement);

// Declare animation variables
var bounceHeight = 0.2; // Adjust the bounce height as needed
var bounceSpeed = 0.04; // Adjust the bounce speed as needed
var time = 0;

var model; // Declare the model variable

// Add an ambient light to provide general illumination
var ambientLight = new THREE.AmbientLight(0xffffff, 0.1); // Color and intensity
scene.add(ambientLight);

// Add directional light
var directionalLight = new THREE.DirectionalLight(0x1998bc, 0.3); // Color and intensity
directionalLight.position.set(1, 1, 1); // Position of the light
scene.add(directionalLight);

// Add directional light from the opposite angle
var directionalLightOpposite = new THREE.DirectionalLight(0xbc1998, 0.5); // Light blue color and intensity
directionalLightOpposite.position.set(-1, -1, -1); // Opposite direction
scene.add(directionalLightOpposite);


var loader = new THREE.GLTFLoader();

loader.load('3d_files/plastic3Dlogo.glb', function (gltf) {
    model = gltf.scene; // Get the root of the imported model

    // Adjust the position, rotation, and scale of the model if needed
    model.position.set(0, 0, 0);
    model.rotation.set(0, (Math.PI / 2) + 180, 0); // Example rotation
    model.scale.set(7, 7, 7); // Example scale

    scene.add(model); // Add the model to the scene

    // Set standard material for the model
    model.traverse(function (child) {
        if (child instanceof THREE.Mesh) {
            // Check if the mesh has materials
            if (child.material) {
                child.material = child.material;
            }
        }
    });
});

camera.position.z = 10; // Adjust camera distance

// Animation loop and rendering here
var animate = function () {
    requestAnimationFrame(animate);

    // Update time for the animation
    time += bounceSpeed;

    // Update the model's vertical position using Math.sin
    if (model) {
        model.position.y = bounceHeight * Math.sin(time) - 0; // Calculate new position
    }

    // Rotate the model around the Y-axis
    if (model) {
        model.rotation.y += 0.01; // Adjust rotation speed as needed
    }

    renderer.render(scene, camera);
};

animate();
